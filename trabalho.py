# Funções aqui

# Descobrir se o número é positivo ou negativo true é negativo e false é positivo
from turtle import pos


def positiveOrNegative(arr):
    if arr[0] == "1":
        return True
    else:
        return False


# C2

# preparação para calculo em C2
def invertBinC2(arr):
    arrTrat = arr[::]
    for i in range(0,len(arr)):
        if arrTrat[i] == "1":
            arrTrat[i] = "0"
        else:
            arrTrat[i] = "1"
        
    return arrTrat

# Soma de 1 unidade para conversão em C2

def addOneC2(arr):
    arrTrat = arr[::-1]
    for i in range(0, 31):
        if arrTrat[i] != "0":
           arrTrat[i] = "0"
        else:
            arrTrat[i] = "1"
            break
                 
    arrTrat = arrTrat[::-1]
    return arrTrat

# Retorna o valor binário em decimal

def toDecimalC2(arr):
    arrTrat = arr[::-1]
    result = 0
    for i in range(0, len(arr)):
        if arrTrat[i] == "1":
            result += 2**i
    return result

# Descobrir qual número é o maior

def biggerNumber(arr1, arr2):
    size1 = 0
    size2 = 0
    arrTrat1 = arr1[::-1]
    arrTrat2 = arr2[::-1]
    for i in range(0,30):
        if arrTrat1[i] == "1":
            size1 += i
        if arrTrat2[i] == "1":
            size2 += i
            
    if size1 >= size2:
        return 0
    if size2 > size1:
        return 1

# Realização da soma

def sumC2(arr1, arr2, judge):
    arrTrat1 = arr1[::-1]
    arrTrat2 = arr2[::-1]

    resto = "0"

    signal1 = arrTrat1.pop()
    signal2 = arrTrat2.pop()

    result = []

    for a in range(0,30):
        if arrTrat1[a] == "1" and arrTrat2[a] =="1":
            if resto == "0":
                result.append("0")
                resto = "1"
            else:
                result.append("1")
                resto = "1"
        else: 
            if  ( arrTrat1[a] == "1" and arrTrat2[a] == "0" ) or ( arrTrat1[a] == "0" and arrTrat2[a] == "1"):
                if resto == "0":
                    result.append("1")
                    resto = "0"
                else:
                    result.append("0")
                    resto = "1"
            else:
                if resto == "0":
                    result.append("0")
                    resto = "0"
                else:
                    result.append("1")
                    resto = "0"

    if judge == 0:
        result.append(signal1)
    else:
        result.append(signal2)

    result = result[::-1]
    return result
            
# Entradas aqui

entry1 = list(input())
entry2 = list(input())
print()

# descobrir sinal
signal1 = positiveOrNegative(entry1)
signal2 = positiveOrNegative(entry2)

# descobrir maior número

biggerNumIs = biggerNumber(entry1, entry2)

# DECIMAL EM C2

if signal1 == True:
    c2Dec1 = invertBinC2(entry1)
    c2Dec1 = addOneC2(c2Dec1)
    print(toDecimalC2(c2Dec1) * -1)
    
else:
    print(toDecimalC2(entry1))
    

if signal2 == True:
    c2Dec2 = invertBinC2(entry2)
    c2Dec2 = addOneC2(c2Dec2)
    print(toDecimalC2(c2Dec2) * -1)
    print()
else:
    print(toDecimalC2(entry2))
    print()

# SOMATORIO E SUBTRAÇÃO EM C2

valueC2sum = sumC2(entry1, entry2, biggerNumIs)
print("".join(valueC2sum))

subC2 = entry2[::]
subC2 = invertBinC2(subC2)
subC2 = addOneC2(subC2)
valueC2sub = sumC2(entry1, subC2, biggerNumIs)
print("".join(valueC2sub))
print()

# DECIMAL DO SOMATORIO E SUBTRAÇÃO EM C2



